import { combineReducers } from 'redux';
import todos from './todos';
import visibilityFilter from './visibilityFiilter';

export default combineReducers({
  todos:todos,
  visibilityFilter:visibilityFilter
});

