import React from 'react';
import { Provider } from 'react-redux';
import { createStore} from 'redux';
import rootReducer from './reducers/index';
import Apps from './components/Apps';

const store = createStore(rootReducer);

function App() {
  return (
  <>
  <Provider store = {store}>
   <Apps/>
  </Provider>
  </>
  );
}

export default App;
