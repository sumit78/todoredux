import {connect} from 'react-redux';
import { setVisibilityFilter} from '../actions/index';
import Link from '../components/Link';

const mapStateTOProps = (state, ownProps) => ({
    active: ownProps.filter === state.visibilityFilter
})

const mapDispatchToprops = (dispatch,ownProps) => ({
    onClick:() => dispatch(setVisibilityFilter(ownProps.filter))
  })

export default connect(
  mapStateTOProps,
  mapDispatchToprops
)(Link)

